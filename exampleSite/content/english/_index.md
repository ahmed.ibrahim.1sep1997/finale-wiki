---
banner:
  title: "ServiceNow Wiki by <br/> <hr> Ejada Servicenow Team"
  description: "ServiceNow Documentation: Internal Comprehensive Guide to Platform Knowledge. Crafted by our team, tailored to our needs."
  image: "images/banner.jpg"
  button:
    enable : true
    label : "Get in touch"
    link : "contact/"

---